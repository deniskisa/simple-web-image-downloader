## Main routine module for downloading images from Web.
#

# necessary includes

# provides the possibility to access cmd line agruments.
# contains exit() function to terminate execution.
import sys
# our class to retrieve images from Web.
import WebImageDownloader


# parameter for command line arguments check.
AllowedCmdArgsNo = 1



if len(sys.argv) !=AllowedCmdArgsNo+1:
   print "Wrong number of parameters. Allowed number:", AllowedCmdArgsNo
   sys.exit() 
else:
   try:
      myDownloader = WebImageDownloader.WebImageDownloader()
   except:
      sys.exit("Failed to create an object of WebImageDownloader class.")
   
   # call main routine to download images.
   myDownloader.downloadImages(sys.argv[1])
   


