Created: Denys Kinshakov, October 26 2015


The folder contains module to retrieve images from the Web and save them
onto local hard disk.

Files:

WebImageDownloader.py				A module to retrieve and store images from Web;
main.py						A module with the main routine;
unittest.py					Unit tests to check the image downloader program;
links.txt					A sample image URLs;
links-empty-file.txt				A sample file with no URLs. For unit testing purposes;
links-not-existing.txt				Fake URLs. Used for unit testing purposes;
links-only-white-spaces.txt			A sample file with no URLs. For unit testing purposes;
links-redundant.txt				A sample image URLs. Some links are repeated;
links-unique.txt				A sample image URLs. No URLs are repeated;
doxyConfigEx					Doxygen configuration file;
doxylog.log					Doxygen log file;
                                                                  

Folders:

doxygen/					Doxygen documentation.


