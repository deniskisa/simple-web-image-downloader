## A module for downloading images from Web.
#

# contains exit() function to terminate execution.
import sys



## URL image retriever program.
#
# The object of this class opens the supplied text file,
# reads off all the URL addresses from it, preprocesses
# these addresses and finaly retrieves the images from
# the supplied addresses. 
class WebImageDownloader:
   

   ## The constructor to properly initialize the object.
   #  @param self The object pointer.
   def __init__(self):

      ## All the content of the supplied file with URL addresses.
      #
      self.mStoredFile = []

      ## Preprocessed, non-repeated URL links.
      #
      self.mSavedUniqueLinks = set()


      ## Number of images downloaded so far.
      #
      self.mDownloadedImagesCount = 0


      ## Character that substitutes slashes from
      #  a file's address in order to be stored onto hard disk.
      self.mSlashReplaceChar = "-"


   ## Opens a file to read of image URLs.
   #  @param self The object pointer.
   #  @param FileWithImageLinks A file with URL addresses.
   #
   #  The routine stores all the content of the supplied file.
   def readFileWithImageUrls(self, FileWithImageLinks):
      try:
         ReadFile=open(FileWithImageLinks,'r')
         self.mStoredFile = ReadFile.readlines()
         ReadFile.close()
      except:
         sys.exit("Could not read the given file")
      #if fileread successful, delete previously stored data.
      self.mDownloadedImagesCount = 0
      self.mSavedUniqueLinks.clear()
      
      



   ## Preprocess the URL addresses.
   #  @param self The object pointer.
   #
   #  Skips all the empty lines, white spaces and repeated addresses.
   def preprocessImageUrls(self):
      for line in self.mStoredFile:
         if len((line.rstrip('\n')).strip()) !=0:
            self.mSavedUniqueLinks.add((line.rstrip('\n')).strip())



   ## Retrieves images from the Web.
   #  @param self The object pointer.
   #  @param slashReplaceChar substitutes slash character in URL address.
   #  that allows to store the image under same name as its URL on a hard disk.
   #
   #  Saves pictures to the local disk. Image URLs serve
   #  as their names except that slas character is changed to some other one.
   def retrieveImagesFromUrl(self, slashReplaceChar=None):
      import urllib # contains file retrieving routines.
      slashReplaceChar=self.mSlashReplaceChar
      self.mDownloadedImagesCount = 0

      for link in self.mSavedUniqueLinks:
         try:
            urllib.urlretrieve(link, link.replace("/", slashReplaceChar))
            self.mDownloadedImagesCount+=1
         except:
            print "Failed to download image at URL address:", link

      print "Downloaded images total:",self.mDownloadedImagesCount

 

   ## The main routine.
   #  @param self The object pointer.
   #  @param pathToFile A file with URL addresses.
   #
   #  Wraps all the necessary function calls. This function is the
   #  interface between a user and the WebImageDownloader.
   def downloadImages(self, pathToFile):
      try:
         self.readFileWithImageUrls(pathToFile)
      except:
         sys.exit("Failed to read file containing image addresses.")
      try:
         self.preprocessImageUrls()
      except:
         sys.exit("Failed to preprocess links to images.")
      try:
         self.retrieveImagesFromUrl()
      except:
         sys.exit("Failed to retrieve images from their URLs.")


