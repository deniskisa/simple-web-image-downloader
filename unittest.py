## Unittest module for WebImageDownloader class.
#

# the class under unit testing.
import WebImageDownloader

## The class with test fixtures and routines to test WebImageDownloader.
#
class WebImageDownloaderTest:


   ## Initializes the test fixtures.
   #  @param self The object pointer.
   def  __init__(self):
      # all the necessary test fixtures.

      ## Address of text file with URL addresses.
      # 
      # Contains several URL samples, both working and not.
      self.mFileWithLinksPath = "links.txt"

      ## Address of text file with URL addresses.
      # 
      # Contains several URL samples, no URL repeated.
      self.mFileWithUniqueLinksPath = "links-unique.txt"

      ## Address of text file with URL addresses.
      # 
      # Contains several URL samples, some repeated.
      self.mFileWithRedundantLinksPath = "links-redundant.txt"

      ## Address of text file with URL addresses.
      # 
      # Contains no addresses, only white spaces.
      self.mFileWithOnlyWhitespacesPath = "links-only-white-spaces.txt"

      ## Address of text file with URL addresses.
      # 
      # Contains no addresses, an empty file.
      self.mEmptyFilePath = "links-empty-file.txt"
      



   ## Tests an object creation.
   #  @param self The object pointer.
   def testCreation(self):
      try:
         DownloaderUnderTest = WebImageDownloader.WebImageDownloader()
         print "Unit testing WebImageDownloader: testCreation passes."
      except:
         print "Unit testing WebImageDownloader: testCreation fails."



   ## Tests an object initialization.
   #  @param self The object pointer.
   def testInitialization(self):
      try:
         DownloaderUnderTest = WebImageDownloader.WebImageDownloader()
         assert len(DownloaderUnderTest.mSavedUniqueLinks) == 0
         assert len(DownloaderUnderTest.mStoredFile) == 0
         assert DownloaderUnderTest.mDownloadedImagesCount == 0
         assert DownloaderUnderTest.mSlashReplaceChar == "-"
         print "Unit testing WebImageDownloader: testInitialization passes."
      except:
         print "Unit testing WebImageDownloader: testInitialization fails."



   ## Tests a text file opening.
   #  @param self The object pointer.
   #
   #  If the object successfully opens the file and stores its content
   #  then it should correspond to the initial content of the file.
   def testOpenFile(self):
      try:
         DownloaderUnderTest = WebImageDownloader.WebImageDownloader()
         DownloaderUnderTest.readFileWithImageUrls(self.mFileWithLinksPath)
         assert DownloaderUnderTest.mStoredFile == (open(self.mFileWithLinksPath, "r")).readlines()
         print "Unit testing WebImageDownloader: testOpenFile passes."
      except:
         print "Unit testing WebImageDownloader: testOpenFile fails."




   ## Tests if links from the file were correctly preprocessed.
   #  @param self The object pointer.
   #
   #  This test case checks whether previously encountered URLs are skipped.
   def testUrlPreprocessing(self):
      try:
         DownloaderUnderTestUnique = WebImageDownloader.WebImageDownloader()
         DownloaderUnderTestUnique.readFileWithImageUrls(self.mFileWithUniqueLinksPath)
         DownloaderUnderTestUnique.preprocessImageUrls()
         DownloaderUnderTestRed = WebImageDownloader.WebImageDownloader()
         DownloaderUnderTestRed.readFileWithImageUrls(self.mFileWithRedundantLinksPath)
         DownloaderUnderTestRed.preprocessImageUrls()
         assert DownloaderUnderTestUnique.mSavedUniqueLinks == DownloaderUnderTestRed.mSavedUniqueLinks
         print "Unit testing WebImageDownloader: testUrlPreprocessing passes."
      except:
         print "Unit testing WebImageDownloader: testUrlPreprocessing fails."




   ## Tests if links from the file were correctly preprocessed.
   #  @param self The object pointer.
   #
   #  This test case checks that white spaces are properly skipped.
   def testUrlPreprocessingWhitespaces(self):
      try:
         DownloaderUnderTest = WebImageDownloader.WebImageDownloader()
         DownloaderUnderTest.readFileWithImageUrls(self.mFileWithOnlyWhitespacesPath)
         DownloaderUnderTest.preprocessImageUrls()
         assert DownloaderUnderTest.mSavedUniqueLinks == set() # means empty set
         print "Unit testing WebImageDownloader: testUrlPreprocessingWhitespaces passes."
      except:
         print "Unit testing WebImageDownloader: testUrlPreprocessingWhitespaces fails."




   ## Tests if files are properly retrieved.
   #  @param self The object pointer.
   #
   #  This test case checks whether previously encountered URLs were skipped.
   def testRetrieveRedundantImage(self):
      try:
         DownloaderUnderTestUnique = WebImageDownloader.WebImageDownloader()
         DownloaderUnderTestUnique.readFileWithImageUrls(self.mFileWithUniqueLinksPath)
         DownloaderUnderTestUnique.preprocessImageUrls()
         DownloaderUnderTestUnique.retrieveImagesFromUrl()
         DownloaderUnderTestRed = WebImageDownloader.WebImageDownloader()
         DownloaderUnderTestRed.readFileWithImageUrls(self.mFileWithRedundantLinksPath)
         DownloaderUnderTestRed.preprocessImageUrls()
         DownloaderUnderTestRed.retrieveImagesFromUrl()
         assert DownloaderUnderTestUnique.mDownloadedImagesCount == DownloaderUnderTestRed.mDownloadedImagesCount
         print "Unit testing WebImageDownloader: testRetrieveRedundantImage passes."
      except:
         print "Unit testing WebImageDownloader: testRetrieveRedundantImage fails."



   ## Tests if files are properly retrieved.
   #  @param self The object pointer.
   #
   #  If there only white spaces and no actual URLs, no images should be downloaded.
   def testRetrieveImageAllPathsAreWhiteSpaces(self):
      try:
         DownloaderUnderTest = WebImageDownloader.WebImageDownloader()
         DownloaderUnderTest.readFileWithImageUrls(self.mFileWithOnlyWhitespacesPath)
         DownloaderUnderTest.preprocessImageUrls()
         DownloaderUnderTest.retrieveImagesFromUrl()
         assert DownloaderUnderTest.mDownloadedImagesCount == 0
         print "Unit testing WebImageDownloader: testRetrieveImageAllPathsAreWhiteSpaces passes."
      except:
         print "Unit testing WebImageDownloader: testRetrieveImageAllPathsAreWhiteSpaces fails."



   ## Tests if files are properly retrieved.
   #  @param self The object pointer.
   #
   #  If the file is empty, no images should be downloaded.
   def testRetrieveImagePathFileIsEmpty(self):
      try:
         DownloaderUnderTest = WebImageDownloader.WebImageDownloader()
         DownloaderUnderTest.readFileWithImageUrls(self.mEmptyFilePath)
         DownloaderUnderTest.preprocessImageUrls()
         DownloaderUnderTest.retrieveImagesFromUrl()
         assert DownloaderUnderTest.mDownloadedImagesCount == 0
         print "Unit testing WebImageDownloader: testRetrieveImagePathFileIsEmpty passes."
      except:
         print "Unit testing WebImageDownloader: testRetrieveImagePathFileIsEmpty fails."


   ## Tests if files are properly retrieved. A standard case
   #  @param self The object pointer.
   #
   #  The plaintext file supplied contains 3 working URLs, so 3 images must be downloaded.
   def testRetrieveImagePathFileStandardCase(self):
      try:
         DownloaderUnderTest = WebImageDownloader.WebImageDownloader()
         DownloaderUnderTest.readFileWithImageUrls(self.mFileWithLinksPath)
         DownloaderUnderTest.preprocessImageUrls()
         DownloaderUnderTest.retrieveImagesFromUrl()
         assert DownloaderUnderTest.mDownloadedImagesCount == 3
         print "Unit testing WebImageDownloader: testRetrieveImagePathFileStandardCase passes."
      except:
         print "Unit testing WebImageDownloader: testRetrieveImagePathFileStandardCase fails."


# create unit test platfor for our class.
myTest =  WebImageDownloaderTest()

# executing all the unit tests.
myTest.testCreation()
myTest.testInitialization()
myTest.testOpenFile()
myTest.testUrlPreprocessing()
myTest.testUrlPreprocessingWhitespaces()
myTest.testRetrieveRedundantImage()
myTest.testRetrieveImageAllPathsAreWhiteSpaces()
myTest.testRetrieveImagePathFileIsEmpty()
myTest.testRetrieveImagePathFileStandardCase()


